const expect = require('chai').expect;
const PetsHelper = require('../pageobjects/petsHelper');

describe('Add new pet', function() {

    it('should add a new pet', async () => {
        const data = PetsHelper.getData();
        const response = await PetsHelper.addNewPet(data);
        expect(response.status).to.equal(200);
        expect(response.data.name).to.equal(data.name);
        expect(response.data.id).to.equal(data.id);
    });

});

describe('Pet', function() {
    let data;

    beforeEach(async () => {
        try {
            data = PetsHelper.getData();
            await PetsHelper.addNewPet(data);
        } catch(error) {
            throw new Error(`An error occured during creating a new pet: ${error}`);
        }
    });

    it('can be received by id', async () => {
        const response = await PetsHelper.getPetById(data.id);
        expect(response.status).to.equal(200);
        expect(response.data.id).to.equal(data.id);
        expect(response.data.name).to.equal(data.name);
    });

    it('should update a pet', async () => {
        const newData = {
            'name': PetsHelper.generateRandomPet(),
            'status': 'unavailable'
        };
        const response = await PetsHelper.updatePet(data.id, newData);
        expect(response.status).to.equal(200);
        expect(response.data.type).to.equal('unknown');
    });

    it('should replace existing pet', async () => {
        const newData = PetsHelper.getData({id: data.id});
        const response = await PetsHelper.replacePet(newData);
        expect(response.status).to.equal(200);
        expect(response.data.id).to.equal(newData.id);
        expect(response.data.name).to.equal(newData.name);
    });

    it('should delete a pet', async () => {
        const response = await PetsHelper.deletePetById(data.id);
        const getPetResponse = await PetsHelper.getPetById(data.id);
        expect(response.status).to.equal(200);
        expect(getPetResponse.status).to.equal(404);
    });

});