const axios = require('axios');

class HttpHandler {
    headers(headers) {
        const basicHeaders = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        };

        return headers ? Object.assign(basicHeaders, headers) : basicHeaders;
    }

    getMethod(url, headers = this.headers()) {
        return axios(
            {
                method: 'get',
                url: url,
                headers: headers,
            },
        ).then((response) => {
            console.log(`GET Response: `, response);
            return response;
        }).catch((error) => {
            console.error(`GET Error: `, error);
            return error.response;
        });
    }

    postMethod(url, data, headers = this.headers()) {
        return axios(
            {
                method: 'post',
                url: url,
                data: data,
                headers: headers,
            },
        ).then((response) => {
            console.log(`POST Response: `, response);
            return response;
        }).catch((error) => {
            console.error(`POST Error: `, error);
            return error.response;
        });
    }
    // async postMethod(url, data, headers = this.headers()) {
    //     try {
    //         const response = await axios(
    //             {
    //                 method: 'post',
    //                 url: url,
    //                 data: data,
    //                 headers: headers,
    //             },
    //         )
    //         console.log(`POST Response: ${JSON.stringify(response)}`);
    //         return response;
    //     } catch (error){
    //         console.error(`POST Error: ${JSON.stringify(error)}`);
    //         return error.response;
    //     }
    // }

    deleteMethod(url, headers = this.headers()) {
        return axios(
            {
                method: 'delete',
                url: url,
                headers: headers,
            },
        ).then((response) => {
            console.log(`DELETE Response: `, response);
            return response;
        }).catch((error) => {
            console.error(`DELETE Error: `, error);
            return error.response;
        });
    }

    putMethod(url, data, headers = this.headers()) {
        return axios(
            {
                method: 'put',
                url: url,
                data: data,
                headers: headers,
            },
        ).then((response) => {
            console.log(`PUT Response: `, response);
            return response;
        }).catch((error) => {
            console.error(`PUT Error: `, error);
            return error.response;
        });
    }
}

module.exports = new HttpHandler();