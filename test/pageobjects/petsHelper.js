import HttpHandler from "./httpHandler";
import {faker} from "@faker-js/faker";
import randomString from "randomstring";

const petstoreBaseURL = 'https://petstore.swagger.io/v2';

class PetsHelper{

    getPetById(id) {
        return HttpHandler.getMethod(`${petstoreBaseURL}/pet/${id}`);
    }

    addNewPet(data) {
        return HttpHandler.postMethod(`${petstoreBaseURL}/pet/`, data);
    }

    replacePet(data) {
        return HttpHandler.putMethod(`${petstoreBaseURL}/pet/`, data);
    }

    updatePet(id, data) {
        const headers = {
            "Content-Type": "application/x-www-form-urlencoded"
        };
        return HttpHandler.postMethod(`${petstoreBaseURL}/pet/${id}`, data, headers);
    }

    deletePetById(id) {
        return HttpHandler.deleteMethod(`${petstoreBaseURL}/pet/${id}`);
    }

    getData({id, name, status} = {}) {
        return {
            'id': id || Number(this.generateNumericString(3)),
            'category': {
                'id': 0,
                'name': 'string'
            },
            'name': name || this.generateRandomPet(),
            'photoUrls': [
                'string'
            ],
            'tags': [
                {
                    'id': 0,
                    'name': 'string'
                }
            ],
            'status': status || 'available'
        };
    }

    generateNumericString(length) {
        return randomString.generate({
            length: length,
            charset: 'numeric'
        });
    }
    generateRandomPet() {
        return `${faker.animal.type()} ${faker.person.firstName()}`;
    }

}

module.exports = new PetsHelper();