# FlyFish-test
Here is a basic example of a test automation framework using WebdriverIO, Mocha and Chai.
Axios is used to make the API calls.
Faker is used to generate random data.

## To run the test, please follow the steps below:
1. Clone the repository
2. Open the project in any IDEA
3. Install the dependencies by running the command `npm install`
4. Run the test by running the command `npm run wdio`